import React from 'react';
import { StyleSheet, Text, Alert, View, FlatList, Modal, Button, TouchableHighlight, Image, Dimensions, WebView, ScrollView } from 'react-native';
import HTML from 'react-native-render-html';

export default class SingleArticle extends React.Component {

    constructor(props) {
        super(props);

        this._onScroll = this._onScroll.bind(this);

        this.state = {
            openWeb: false,
            isVisible: false,
        }
    }



    _onScroll(e) {
        var windowHeight = Dimensions.get('window').height,
            height = e.nativeEvent.contentSize.height,
            offset = e.nativeEvent.contentOffset.y;
        // Alert.alert('ok');
        if (windowHeight + offset >= height) {
            // Alert.alert('End Scroll')
            this.setState({
                openWeb: true,
                isVisible: true
            });
        }
    }

    static navigationOptions = {
        title: "Back",
        header: null
    }


    render() {

        return (
            <ScrollView
                // onScroll={() => this.refs.modal2.open()} style={styles.btn}
                style={styles.btn}
                onScroll={this._onScroll}

                style={{ backgroundColor: 'white' }}
                scrollEventThrottle={16}
            >
                <Modal
                    visible={this.state.isVisible}
                    animationType="slide"
                    style={{ margin: 0 }}
                >
                    <View style={{ flex: 1 }}>
                        <TouchableHighlight
                            onPress={() => {
                                this.setModalVisible(!this.state.isVisible);
                            }}>
                            <Text>Hide Modal</Text>
                        </TouchableHighlight>
                        <WebView
                            source={{ uri: this.props.navigation.state.params.item.source_url }}
                            style={{ marginTop: 20, flex: 1 }}
                        />
                    </View>
                </Modal>

                <Image
                    source={{ uri: this.props.navigation.state.params.item.jetpack_featured_media_url }}
                    style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').width * 0.72
                    }}
                />
                <Text style={{ fontSize: 20, padding: 20, fontWeight: 'bold' }}>{this.props.navigation.state.params.item.title.rendered}</Text>

                <HTML html={this.props.navigation.state.params.item.content.rendered} imagesMaxWidth={Dimensions.get('window').width}
                    containerStyle={{
                        padding: 20,
                    }}
                    tagsStyles={{ p: { fontSize: 16, color: 'grey', paddingBottom: 20 }, strong: { fontWeight: 'bold' } }}
                />
                <Button title="Press Me">Basic modal</Button>

                {/* <Modal style={[styles.modal, styles.modal]} backdrop={false} position={"top"} ref={"modal2"}>
                    <WebView
                        source={{ uri: this.props.navigation.state.params.item.source_url }}
                        style={{ marginTop: 20, flex: 1, width: 450, height: 100 }} />

                </Modal> */}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({

    wrapper: {
        paddingTop: 50,
        flex: 1
    },

    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },

    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10,
        marginTop: 22
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },

    text: {
        color: "black",
        fontSize: 22
    }

});