import React from 'react';
import { StyleSheet, Text, Alert, View, FlatList, Button, TouchableOpacity, Image, Dimensions, WebView, ScrollView } from 'react-native';
import Modal from "react-native-modal";

import HTML from 'react-native-render-html';

export default class SingleArticle extends React.Component {
    state = {
        visibleModal: null,
    };

    _toggleModal2 = () =>
        this.setState({ visibleModal: !this.state.visibleModal });


    _toggleModal(e) {
        var windowHeight = Dimensions.get('window').height,
            height = e.nativeEvent.contentSize.height,
            offset = e.nativeEvent.contentOffset.y;
        // Alert.alert('ok');
        if (windowHeight + offset >= height) {
            // Alert.alert('End Scroll')
            this._toggleModal2;

        }
    }

    render() {
        return (
            <ScrollView
                // onScroll={() => this.refs.modal2.open()} style={styles.btn}
                onScroll={this._toggleModal}
                style={{ backgroundColor: 'white' }}
                scrollEventThrottle={100}
            >

                <Image
                    source={{ uri: this.props.navigation.state.params.item.jetpack_featured_media_url }}
                    style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').width * 0.72
                    }}
                />
                <Text style={{ fontSize: 20, padding: 20, fontWeight: 'bold' }}>{this.props.navigation.state.params.item.title.rendered}</Text>
                {/* <WebView
                    source={{ html: this.props.navigation.state.params.item.content.rendered }}
                /> */}
                <HTML html={this.props.navigation.state.params.item.content.rendered} imagesMaxWidth={Dimensions.get('window').width}
                    containerStyle={{
                        padding: 20,
                    }}
                    tagsStyles={{ p: { fontSize: 16, color: 'grey', paddingBottom: 20 }, strong: { fontWeight: 'bold' } }}
                />

                <TouchableOpacity onPress={this._toggleModal2}>
                    <Text>Show Modal</Text>
                </TouchableOpacity>
                <Modal
                    isVisible={this.state.visibleModal}
                    backdropColor={"red"}
                    backdropOpacity={1}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={1000}
                    animationOutTiming={1000}
                    backdropTransitionInTiming={1000}
                    onSwipeStart={() => this.setState({ visibleModal: !this.state.visibleModal })}
                    swipeDirection="down"
                    hideModalContentWhileAnimating={true}
                // swipeThreshold={90}
                >
                    <View style={{ flex: 1, backgroundColor: 'red' }}>
                        <TouchableOpacity onPress={this._toggleModal2}>
                            <Text>Hide me!</Text>
                        </TouchableOpacity>
                        <WebView source={{ uri: this.props.navigation.state.params.item.source_url }} bounces={false} style={{ flex: 1 }} />
                    </View>
                </Modal>
            </ScrollView >
        )
    }
}

const styles = StyleSheet.create({

    wrapper: {
        paddingTop: 50,
        flex: 1
    },

    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },

    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },

    text: {
        color: "black",
        fontSize: 22
    }

});