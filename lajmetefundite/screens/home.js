import React from 'react';
// import ListLajmesh from '../components/ListeLajmesh';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image, Dimensions } from 'react-native';
import { List, ListItem, SearchBar } from "react-native-elements";


export default class Home extends React.Component {
    static navigationOptions = {
        title: "Welcome",
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            page: 1,
            seed: 1,
            error: null,
            refreshing: false,
            isOpen: false,

        };
    }



    componentDidMount() {
        this.makeRemoteRequest();
    }

    makeRemoteRequest = () => {
        const { page, seed } = this.state;
        const url = `https://lajmetefundit.al/wp-json/wp/v2/posts`;
        this.setState({ loading: true });

        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    data: page === 1 ? res : [...this.state.data, ...res],
                    error: res.error || null,
                    loading: false,
                    refreshing: false
                });
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });
    };

    handleRefresh = () => {
        this.setState(
            {
                page: 1,
                seed: this.state.seed + 1,
                refreshing: true
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    handleLoadMore = () => {
        this.setState(
            {
                page: this.state.page + 1
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    };

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round />;
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return (
            <View style={styles.container} >
                <Text style={styles.h2text}>
                    Black Order
          </Text>
                <TouchableOpacity
                    onPress={() => this.props.navigation.openDrawer()}>
                    <Text>TOGGLE</Text>
                </TouchableOpacity>
                <FlatList
                    data={this.state.data}
                    showsVerticalScrollIndicator={false}
                    // renderItem={({ item }) =>
                    //   <View style={styles.flatview}>
                    //     <Text style={styles.name}>{item.title.rendered}</Text>
                    //     <Text style={styles.email}>{item.email}</Text>
                    //   </View>
                    // }
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{
                            paddingVertical: 4,
                            marginLeft: 4,
                            borderTopWidth: 0.3,
                            borderColor: "#CED0CE",
                            // flex: 1
                        }}

                            activeOpacit='1'
                            onPress={() => this.props.navigation.navigate('SingleArticle', { item: item })}
                        >
                            <View style={styles.MainContainer}>

                                <View style={styles.Majtas}>
                                    <Image
                                        source={{ uri: item.jetpack_featured_media_url }}
                                        style={{
                                            width: 100,
                                            height: 100,
                                            backgroundColor: "#CED0CE",
                                            marginLeft: 0,
                                            flex: 1
                                        }}
                                    />
                                </View>
                                <View style={styles.Djathtas}>
                                    <Text style={styles.DjathtasText}>{item.title.rendered}</Text>
                                    <Text style={styles.DjathtasTextPortal}>{item.portali}</Text>

                                </View>

                            </View>
                        </TouchableOpacity>


                    )
                    }
                    keyExtractor={(item, index) => index.toString()
                    }

                />
            </View >
        );
    }
}


const styles = StyleSheet.create({
    MainContainer: {
        // flex: 1,
        // backgroundColor: 'red',
        flexDirection: 'row',
        // justifyContent: 'space-between',

    },
    Majtas: {
        // width: 100,
        backgroundColor: 'green',
        height: 100
    },
    Djathtas: {
        // backgroundColor: 'red',
        width: Dimensions.get('window').width - 100, //full width
        // width: 100,
        height: 100,
        justifyContent: 'flex-start'
    },
    DjathtasText: {
        fontSize: 15,
        padding: 15,
        // flex: 1
    },
    DjathtasTextPortal: {
        flex: 1,
        flexDirection: 'row', flexWrap: 'wrap',
        fontSize: 9,
        fontWeight: 'bold',
        padding: 15,
        flex: 1,
        color: 'grey'
    }

});