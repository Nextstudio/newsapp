import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createDrawerNavigator, createAppContainer } from "react-navigation";
import Home from './screens/home';
import SingleArticle from './screens/SingleArticle2';

const AppNavigator = createDrawerNavigator({
  Home: { screen: Home },
  SingleArticle: { screen: SingleArticle }
});

export default createAppContainer(AppNavigator);
